---
layout: 2022/post
section: proposals
category: misc
author: César García
title: Open Data Engineering Framework v0.1
---

# Open Data Engineering Framework v0.1

> En este artículo, exploraremos Open Data Engineering Framework (ODEF), una metodología para contribuir a la mejora de los datos abiertos desde la ciudadanía utilizando herramientas libres.<br><br>
En los últimos años, estamos asistiendo a múltiples iniciativas particulares y/o privadas que tratan de exponer problemas en el acceso a los datos abiertos, la calidad o completitud de los mismos. En múltiples casos, estas iniciativas son de tipo individual y no se basan en la colaboración entre múltiples personas o grupos.<br><br>
ODEF parte desde un punto de vista complementario: tratando de generar procesos colaborativos, importando las mejores prácticas de la ingeniería de datos. Para ello se propone la generación de entornos que utilicen herramientas libres y que puedan ser fácilmente replicables, de modo que cualquier persona pueda contribuir a mejorar los datos abiertos que se están ofreciendo en la actualidad desde distintas fuentes. En el artículo explicaremos cuáles son los principios generales.

## Detalles de la propuesta:

-   Tipo de propuesta: Artículo

-   Descripción:

> En este artículo expondremos los distintos elementos que componen el marco de trabajo ODEF v0.1. En primer lugar exploraremos cuáles son las distintas iniciativas que están trabajando en el campo de los datos abiertos en España y cuáles son los principales problemas que se están encontrando para permitir una reutilización profesional de los mismos.<br><br>
A continuación, expondremos en que consiste la ingeniería del dato y cuáles son las mejores prácticas que podemos importar para mejorar la utilización de los datos abiertos.  Todo este andamiaje técnico no tiene sentido alguno si no hay personas implicadas en su aplicación. Por ello, expondremos también cuál puede ser el papel de la ciudadanía en la observación, vigilancia y mejora de los datos abiertos, partiendo de los principios de la cibernética.<br><br>
Por último, detallaremos el stack tecnológico que daría forma a una solución de este tipo, empleando herramientas libres, de tal forma que cualquier persona interesada pueda contribuir de forma positiva en la mejora del ecosistema open data. Se expondrán diversos ejemplos de aplicación, para ofrecer patrones replicables colaborativos.

-   Público objetivo:

> Personas interesadas en el análisis de datos, ingeniería de datos y/o los datos abiertos. También puede resultar interesante a gente interesada en mejorar la calidad de los datos abiertos que se ofrecen desde las distintas administraciones estatales, autonómicas o municipales.

## Ponente:

-   Nombre: César García

-   Bio:

> Mi nombre es César García. Durante más de diez años he trabajado para la administración, antes de abandonar este rol para dedicarme a investigar cómo impactan las nuevas tecnologías en la sociedad.<br><br>
Durante el último año me he enfocado en la ciencia de datos y cómo se puede aplicar a conjuntos de datos abiertos. Para mi sorpresa, muchos de estos conjuntos de datos contienen errores tremendos, que los hacen inutilizables. ¿Qué valor tiene la generación de datos abiertos si este proceso no se realiza de forma rigurosa?<br><br>
Multiples iniciativas están abordando este problema desde una perspectiva individual, pero ¿cómo podría resolverse de forma colectiva? Esta pregunta me ha llevado a explorar los principios de la ingeniería de datos, para preparar una propuesta que me gustaría compartir con cualquier persona interesada en esta cuestión. De momento, no es más que un borrador, que me gustaría compartir por primera vez en esLibre, al ser un público que pienso que puede estar interesado en este tipo de iniciativas cívicas.

### Info personal:

-   Web personal: <https://elsatch.github.io>
-   Twitter: <https://twitter.com/elsatch>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/elsatch> / <https://gitlab.com/elsatch>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
