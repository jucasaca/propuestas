---
layout: 2022/post
section: proposals
category: workshops
author: Rubén Rodríguez Pérez
title: Hacking neuronal&#58 uso de realidad virtual libre para el tratamiento del "ojo vago"
---

# Hacking neuronal: uso de realidad virtual libre para el tratamiento del "ojo vago"

> Introducir un proyecto para el tratamiento de la ambliopía usando software de realidad virtual libre presenta una oportunidad para indagar en la naturaleza de este desorden neuronal tan común y para analizar el estado del software libre en el ámbito de la realidad virtual así como en el de la creación de videojuegos libres en general.<br><br>
Siendo una persona con estrabismo y ambliopía, y al que como a casi todos me dijeron al llegar a la pubertad que el problema no tenía arreglo, cuando descubrí nuevas vías de tratamiento hace 8 años intenté desarrollar mi propio sistema de terapia. Un rotundo fracaso en aquel momento debido al temprano estado de la tecnología de VR y de programación de videojuegos en GNU/Linux, pero una realidad alcanzable en la actualidad. Esta charla muestra el prometedor estado del sector del desarrollo de videojuegos en GNU/Linux y de su uso con visores VR.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial

-   Descripción:
> Salvo por una breve introducción a los aspectos biológicos de estos desórdenes visuales, la charla se centra principalmente en el desarrollo de videojuegos usando software libre (Godot) el estado del VR en GNU/Linux (Monado), y la necesidad de uso de licencias libres para el software médico en general.<br><br>
El taller combina una parte de charla con presentación, de unos 30 minutos, seguida de una demo práctica de VR en GNU/Linux, desarrollo de videojuegos 3D y VR con Godot, y el ejemplo específico de los programas de tratamiento de la ambliopía que forman parte del proyecto presentado. La demo podría durar otros 30 minutos, seguidos de amplio espacio para preguntas, para mostrar el entorno de desarrollo Godot, e incluso para que el público pueda probar el visor VR si lo desean. En total es posible que se utilicen las 2 horas de máximo, dependiendo del interés del público.<br><br>
Expuesto desde el punto de vista de un desarrollador y activista, pues no soy médico. El proyecto, que está en sus inicios, fue presentado en la conferencia Libreplanet 2022 en una charla disponible en <https://media.libreplanet.org/u/libreplanet/m/hacking-my-brain-free-virtual-reality-implementations-and-their-potential-for-therapeutic-use/><br><br>
Más información del proyecto está disponible en <https://gitlab.trisquel.org/vr/documentation> así como dos proyectos para Godot 3.x OpenXR: un clon del videojuego clásico breakout en 3D para VR, y un visor de películas 3D en espacio VR.<br><br>
Para presentar el taller tendré que hacer uso de hardware específico: un PC de sobremesa y un visor de realidad virtual, de los cuales dispongo y llevaría al congreso. El PC tiene salida HDMI, Display-port, y DVI, todos de tamaño completo. Idealmente también se puede conectar a altavoces durante la demo.

-   Web del proyecto: <https://gitlab.trisquel.org/vr/>

-   Público objetivo:

> Desarrolladores de videojuegos, edición de gráficos 3D, sistemas de realidad virtual, sistemas de procesamiento de video en tiempo real (tracking de espacios mediante cámaras estereoscópicas), activistas de licencias (en particular las de uso en tecnología médica e investigación científica), personas con discapacidades visuales o neurológicas.

## Ponente:

-   Nombre: Rubén Rodríguez Pérez

-   Bio:

> Fundador del proyecto Trisquel GNU/Linux, mantenedor de paquetes del proyecto GNU, ex-director técnico de la Free Software Foundation. Gallego e ingeniero de software por la Universidad de Vigo, actualmente centrado en el desarrollo del proyecto Trisquel y del proyecto de tratamiento de ambliopía presentado en este congreso.

### Info personal:

-   Web personal: <https://quidam.cc/projects-portfolio>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://gitlab.trisquel.org/ruben>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
