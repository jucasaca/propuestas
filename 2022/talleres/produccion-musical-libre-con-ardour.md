---
layout: 2022/post
section: proposals
category: workshops
author: Cipriano Montes Aranda
title: Producción musical libre con Ardour
---

# Producción musical libre con Ardour

> "Me gustaría pasarme al software libre, pero los programas de producción audiovisual todavía no están a la altura".<br><br>
"¡Quiero hacer música con el ordenador, pero no tengo ganas de pagar por programas opacos ni de hacer filigranas para piratearlos y que funcionen bien!"<br><br>
Este taller pretende derribar estas barreras tan comunes presentando el proyecto de Ardour, una estación de trabajo de audio digital publicada bajo la licencia GPL.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial

-   Descripción:

> Quiero explicar lo que me hubiese gustado saber hace años: es posible producir música de calidad en GNU/Linux, y es posible que sea incluso más fácil que en otros sistemas. El taller tendrá una parte teórica y una parte práctica:<br><br>
1. Presentación de Ardour [duración estimada: menos de 30 minutos]:
  * Configuración previa del sistema operativo: JACK, cadence, catia, LilyPond...
  * Recorrido por las funciones principales de Ardour: grabación multipista, MIDI, sintetizadores, plug-ins...
  * Demostración de la potencia de Ardour: breve análisis de la sesión de algún proyecto terminado.<br><br>
2. Producción guiada de una pequeña sesión de Ardour [duración: algo más de 30 minutos, en función del público asistente].

-   Web del proyecto: <https://ardour.org/>

-   Público objetivo:

> Cualquier persona interesada en hacer música con su ordenador utilizando exclusivamente software libre. No es necesario disponer de ningún equipamiento extra para seguir la parte práctica del taller: es posible introducir notas MIDI con el teclado integrado del portátil o con un ratón. Sí que sería aconsejable tener una instalación de Ardour preparada para la parte práctica.

## Ponente:

-   Nombre: Cipriano Montes Aranda

-   Bio:

> Estudié Filosofía en la UGR y Composición musical en el RCSM Victoria Eugenia, y de forma paralela me dediqué a trastear con ordenadores. Lo que sé de software libre, de producción y de Ardour lo aprendí por mi cuenta, y tengo muchas ganas de compartirlo y de aprender del resto de participantes de esta edición de esLibre.

### Info personal:

-   Web personal: <https://cipriano.es/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
