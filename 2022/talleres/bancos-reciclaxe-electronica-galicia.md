---
layout: 2022/post
section: proposals
category: workshops
author: José Antonio González Nóvoa, Martina Páramos Rial, José Carlos Mourelos Peteiro
title:  Bancos de Reciclaxe Electrónica de Galicia con Software Libre
---

#  Bancos de Reciclaxe Electrónica de Galicia con Software Libre

> A fenda dixital, que xa estaba aí antes da pandemia, fíxose aínda máis visible. Moitas familias tivérono complicado para poder manter o ritmo en ámbitos no que o dixital amosouse como un xeito de non desconectar do mundo. A educación infantil foi un deles, e lamentablemente en moitos fogares non se puideron seguir as clases coma no resto. Por iso, varias asociacións do ámbito tecnolóxico (Enxeñería Sen Fronteiras, Aberteo, IEEE UVigo, Informática Solidaria e A Industriosa) quixemos unir forzas para poder fechar esa fenda dixital, tratando ademais de promover a formación dixital e a reutilización de equipos que aínda poderían ser útiles, atrasando a súa obsolescencia, e promovendo o emprego de software libre.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Descripción:

> A idea para o congreso é dar unha charla / obradoiro explicando primeiro brevemente o que facemos para a continuación dar un obradoiro mostrando un exemplo de reparación dun equipo informático para que a xente lle perda o medo a cacharrear con eles. Estimamos unha duración aproximada dunha hora. Necesitaríamos dipoñer de conexión a internet e unha fonte de alimentación.

-   Web del proyecto: <https://galicia.isf.es/bancos-de-reciclaxe-electronica-con-software-libre/>

-   Público objetivo:

> Persoas con interese en proxectos de carácter más social e especialmente aquelas que non teñen coñecementos sobre Linux e reparación de ordenadores, ainda que está aberto a todo tipo de público.

## Ponente:

-   Nombre: José Antonio González Nóvoa

-   Bio:

> Son voluntario e socio das asociacións Enxeñería Sen Fronteiras, IEEE UVigo e a Industriosa. Xa teño dado presentacións relacionadas con este tema dentro da Universidade, asi como noutros eventos.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
