---
layout: 2022/post
section: proposals
category: workshops
author: JJ Merelo
title: Lenguajes de programación posmodernos
---

# Lenguajes de programación posmodernos

> Esto es, en realidad, una excusa para hablaros de Raku. Con una primera versión en el año 2015, este lenguaje viene a incluir una serie de conceptos que aparecen, por separado, en todos los lenguajes actuales, desde soporte de Unicode hasta programación concurrente, desde programación funcional hasta programación orientada a objetos, metaprogramación y soporte para mini-lenguajes.<br><br>
En resumen, un lenguaje que merece la pena conocer, aunque sea de paso, y que te permite conocer mejor qué se cuece en donde se diseñan los lenguajes de programación actualmente.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial

-   Descripción:

> La programación actual incluye una serie de técnicas, conceptos y buenas prácticas, que resulta difícil encontrar en lenguajes específicos; sin embargo, están surgiendo una serie de lenguajes multi-paradigma que permiten escoger tu propio estilo de escribir código y llevarlo a producción como mejor te apetezca. Esta charla pretende ser una introducción a diferentes conceptos de programación actuales, con ejemplos en diferentes lenguajes, y por supuesto en Raku. Pretende animar tanto a los que quieran conocer nuevos lenguajes, como a aquellos que quieran ir más allá del monolingüismo y pretendan entender conceptos que pueden, eventualmente, aterrizar en tu lenguaje preferido (u odiado, pero que te da de comer)

-   Web del proyecto: <https://raku.org>

-   Público objetivo:

> Personas con conocimientos de programación, y curiosidad por nuevos lenguajes.

## Ponente:

-   Nombre: JJ Merelo

-   Bio:

> Profesor de la UGR, core developer en Raku e ingeniero software senior en polypoly.

### Info personal:

-   Web personal: <https://jj.github.io/cv>
-   Twitter: <https://twitter.com/jjmerelo>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://gitlab.com/JJ5>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
