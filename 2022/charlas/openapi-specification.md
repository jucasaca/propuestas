---
layout: 2022/post
section: proposals
category: talks
author: Benjamin Granados
title: OpenAPI Specification&#58 Presente y futuro
---

# OpenAPI Specification: Presente y futuro

> En la charla presentaremos los elementos fundamentales de OpenAPI Specification y explicaremos el papel crítico que juega a la hora de crear y/o integrar APIs. Por último repasaremos el presente y futuro del standard.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial

-   Descripción:

> En la charla presentaremos los aspectos fundamentales de OpenAPI Specification y explicaremos el papel crítico que juega a la hora de crear y/o integrar APIs. Presentaremos también OpenAPI Initiative bajo el paraguas de Linux Foundation y por último repasaremos el presente y futuro del standard.

-   Web del proyecto: <https://spec.openapis.org/oas/latest.html>

-   Público objetivo:

> Ingenieros de Software, Desarrolladores, Arquitectos y cualquier persona involucrada en el diseño, desarrollo o integración de APIs.

## Ponente:

-   Nombre: Benjamin Granados

-   Bio:

> Developer Evangelist en Twilio y apasionado de las APIs comunidades de Tecnología. Mi pasión son las APIs y actualmente soy miembro del comité de API Addicts. Puedes contactarme en Twitter en @benjagm. Cuando no estoy con las manos en las APIs me encanta escalar montañas.

### Info personal:

-   Twitter: <https://twitter.com/@benjagm>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/benjagra>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
