---
layout: 2022/post
section: proposals
category: talks
author: Pablo Castro Valiño
title: Creando comunidades locales, ¿algún consejo?
---

# Creando comunidades locales, ¿algún consejo?

> Las comunidades tecnológicas son cada día son más importantes en el sector tech pero como sabemos todos lo que hemos estado en ellas, mantenerlas es una tarea muy dura, que implica muchos sacrificios y trabajo en nuestro tiempo libre. ¿Algún consejo para mantenerlas?

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> Las comunidades tecnológicas son cada día son más importantes en el sector tech pero como sabemos todos lo que hemos estado en ellas, mantenerlas es una tarea muy dura, que implica muchos sacrificios y trabajo en nuestro tiempo libre.<br><br>
Es por eso que en esta charla me gustaría contar mis experiencias durante más de 10 años participando, liderando e incluso construyendo múltiples comunidades tecnológicas locales, con el objetivo de compartir mis pequeños trucos y aprendizajes para crear "engagement", intentar implicar a más gente y en general gestionar una comunidad de forma satisfactoria.

-   Público objetivo:

> Organizadores de comunidades tecnológicas y personas que contribuyan a dichas comunidades.

## Ponente:

-   Nombre: Pablo Castro Valiño

-   Bio:

> Frontend Developer y co-fundador de Arengu, entusiasta de la cultura libre y activista por la libertad de internet. Fuerte defensor del software, hardware y cultura libre a través de la asociación GPUL, gestionando e impartiendo charlas, hackathones y conferencias en comunidades de software libre. También llevo varios años trabajando en privacidad y soberanía tecnológica a través de Trackula y he coorganizando AtlanticaConf, la mayor conferencia tech de Galicia.

### Info personal:

-   Web personal: <https://www.techtopias.com/>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/web/@castrinho8>
-   Twitter: <https://twitter.com/castrinho18>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/castrinho8>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
