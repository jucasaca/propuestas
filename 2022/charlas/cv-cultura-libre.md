---
layout: 2022/post
section: proposals
category: talks
author: Pablo Hinojosa Nava
title: El CV de la cultura libre
---

# El CV de la cultura libre

> Actualmente no existe una manera común de recopilar todas nuestras contribuciones a cualquier comunidad. Si eres desarrollador puedes mostrar tu perfil de Github/Gitlab, si eres editor de un proyecto Wikimedia puedes mostrar tu página de perfil pero ¿qué tal si reunimos todas esas contribuciones en un CV de la cultura libre? Se analizará el estado actual, posibilidades y un esbozo de la implementación técnica para tener un CV autogenerado.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial

-   Descripción:

> ¿Eres colaborador de alguna comunidad de la cultura libre? ¿en cuáles colaboras? ¿qué has hecho? Existen docenas de comunidades libres en las que puedes colaborar, pero no hay un lugar único donde puedas contar y mostrar tus contribuciones. Hay aproximaciones como linktr.ee o keybase que te permiten recopilar enlaces, pero no existe un CV de contribuciones a la cultura libre.<br><br>
Analizaremos y debatiremos una propuesta (evidentemente basada en software libre) para generar este CV. De esta manera podremos mostrar nuestras contribuciones tanto en términos cuantitativos como en términos cualitativos en las diversas plataformas que existen en internet.<br><br>
Este CV también servirá a reclutadores para conocer de una manera más rápida a los candidatos y poder evaluar en base a sus contribuciones la idoneidad de cada uno para el puesto. A continuación dejamos un listado de plataformas de cultura libre que pueden ser incluidas en el CV ¿se te ocurre alguna más?
* GitHub
* Gitlab
* Wikimedia (Wikimedia, Wikidata, Commons...)
* openfoodfacts.org / openbeautyfacts.org
* openstreetmap.org
* musicbrainz.org
* fandom.com
* Sourceforge
* ebird.org
* cience.nasa.gov/citizenscience
* doaj.org
* code.gov

-   Público objetivo:

> Cualquier contribuidor a la cultura libre. Reclutadores de personal / captadores de talento informático (o no)

## Ponente:

-   Nombre: Pablo Hinojosa Nava

-   Bio:

> Pablo Hinojosa Nava es un miembro activo y usual asistente a eventos relacionados con la cultura libre. Miembro de Wikimedia España, Civio, miembro vitalicio de Maldita.es... entre otras cosas. Trabaja en una empresa que desarrolla software libre, trabajó en otra que utilizaba software libre en los hospitales y montó la plataforma técnica de otra con software libre.<br><br>
Ha contribuido en varias plataformas de las que aparecen en la lista superior, a veces como Pablo Hinojosa Nava, a veces como Pablohn6, a veces como Pablohn26...

### Info personal:

-   Web personal: <https://github.com/Pablohn26>
-   Twitter: <https://twitter.com/pablohn6>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://gitlab.com/Pablohn26>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
