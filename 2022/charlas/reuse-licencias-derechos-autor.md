---
layout: 2022/post
section: proposals
category: talks
author: Lina Ceballos
title: REUSE&#58 Indicar licencias y derechos de autor nunca fue tan sencillo
---

# REUSE: Indicar licencias y derechos de autor nunca fue tan sencillo

> Desarrollar, usar y re-usar Software Libre es divertido, pero lidiar con la información sobre licencias y derechos de autor no lo es. El proyecto REUSE cambia eso. Con tres sencillos pasos, hace que añadir y leer la información sobre licencias y derechos de autor sea fácil tanto para los humanos como para las máquinas. En esta presentación, Lina Ceballos nos guiará a través de los principios de REUSE y nos presentará cómo hacer que las licencias sean claras y sencillas.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> Si quieres conceder a los usuarios la libertad de usar, estudiar, compartir y mejorar tu software, tienes que conceder esas libertades en la licencia del software. Para animar a la gente a desarrollar Software Libre, ayudamos a los desarrolladores a entender y aplicar las licencias de Software Libre. Desde el 2017, REUSE contribuye a este objetivo. Cualquier proyecto que siga las recomendaciones de la iniciativa hace que la información sobre derechos de autor y licencias sea legible para ambos: humanos y máquinas. De esta manera, queremos asegurar que los individuos, organizaciones y empresas que están reutilizando el código sean conscientes de los términos de la licencia elegida por el autor original.<br><br>
REUSE no "reinventa la rueda". Por el contrario, se integra perfectamente en los procesos de desarrollo y en otras buenas prácticas al momento de indicar licencias de Software Libre. Además, hay herramientas y documentación para ayudarte a empezar. Durante esta charla veremos más de cerca estas herramientas y documentación, con el bonus de ver una demostración en vivo de cómo hacer que un proyecto cumpla con las especificaciones de REUSE.

-   Web del proyecto: <https://reuse.software/>

-   Público objetivo:

> Esta charla es de ayuda para cualquier persona que utilice Software Libre, no se requiere de conocimiento técnico,  ni necesitas ser un(a) desarrollador(a) de software. Las licencias y derechos de autor de los proyectos de Software Libre que usamos, nos incumbe a todos. Si eres un(a) desarrollador(a), lo más probable es que esta charla cambie tu forma de indicar esta información legal en tu proyecto ;)

## Ponente:

-   Nombre: Lina Ceballos

-   Bio:

> Lina ha estudiado Derecho y tiene un Máster en Ciencia Política. Ahora forma parte del equipo jurídico y político de la FSFE. Ha adquirido experiencia en la defensa de derechos digitales frente a responsables de la toma de decisiones y de las administraciones públicas. Cuenta con experiencia en asuntos legales y en el cumplimiento de licencias, así como en "community building". Contribuye a la iniciativa ¿Dinero Público? ¡Código Público!, al proyecto REUSE y a la campaña Router Freedom.

### Info personal:

-   Twitter: <https://twitter.com/@lnceballosz>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/lnceballosz>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
