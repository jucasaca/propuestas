---
layout: 2022/post
section: proposals
category: talks
author: Florencia Claes, Francisco Gortázar y Jesús González Barahona
title: La OfiLibre de la URJC&#58 un modelo para fomentar la cultura libre en la Universidad
---

# La OfiLibre de la URJC: un modelo para fomentar la cultura libre en la Universidad

> La Oficina de Conocimiento y Cultura Libre de la Universidad Rey Juan Carlos (OfiLibre de la URJC) fue creada en la URJC hace unos años para promover el software libre y la cultura libre en la Universidad. Su misión es informar, promocionar, coordinar y facilitar, en sus ámbitos de actuación, y en colaboración con otros agentes de la Universidad. Este modelo de oficina no normativa, que tiene que trabajar con el resto de la comunidad universitaria, y que tiene un ámbito de actuación amplio, que incluye el software libre pero también otros dentro del marco más amplio de la cultura libre, está empezando a dar sus frutos. En esta presentación mostraremos cuál es nuestro modelo, por qué se ha diseñado de esta forma, y qué tal está funcionando.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Descripción:

> La [OfiLibre](https://ofilibre.gitlab.io) de la [URJC](https://urjc.es) empezó a funcionar en 2019. Aunque su modelo está claramente basado en las Oficinas de Software Libre que ha habido, y sigue habiendo, en muchas universidades españolas, tiene una diferencia clara con casi todas ellas: sus ámbitos de actuación son, además del software libre, los datos abiertos, la publicación en acceso abierto, y la cultura libre en general. Esta ampliación más allá del software libre ha permitido que la OfiLibre tenga un papel mucho más central del habitual, y mucha más relación con los agentes de la comunidad universitaria que normalmente no se sienten implicados en temas relacionados con al informática.<br><br>
La vocación de la OfiLibre es "informar, promocionar, coordinar y facilitar". Esta vocación no normativa también ha tenido implicaciones muy importantes para su funcionamiento. Aunque la OfiLibre participa en la creación de reglamentos y normativas, estos han de discutirse previamente, y delimitarse, en el Consejo de Publicación Abierta, un órgano de la Universidad diseñado específicamente para orientar la política de la Universidad en el ámbito de la cultura libre. En él se tienen en cuenta las opiniones e intereses de los diferentes agentes de la Universidad, y se busca que lo que se decida sea un consenso aceptable para todas las partes, lo que facilita mucho el trámite en los órganos decisorios que corresponda. Además, esto facilita tanto el conocimiento de la política con respecto a la cultura libre en toda la Universidad, y la identificación de sinergias con cualquier agente en ella. Esto también obliga a una fuerte coordinación con los agentes ejecutivo (vicerrectorados, servicios, gerencias, etc.) que ha resultado muy conveniente porque facilita la creación de canales de comunicación fluidos, y la puesta en marcha de acciones de coordinación.<br><br>
Las acciones de la OfiLibre, por lo tanto, se están centrando el el fomento de la colaboración entre los agentes con competencias en los diversos aspectos de la cultura libre, en la identificación de oportunidades, acciones y proyectos acometibles, y de interés para la comunidad universitaria, y en la información y la formación sobre los temas de su competencia.<br><br>
Creemos que el modelo de la OfiLibre está consiguiendo extender con cierto éxito el modelo más tradicional de las oficinas de software libre, y que presenta algunas ventajas que podrían ser de utilizad en otras Universidades. Sin embargo, después de estos años de funcionamiento, también hemos identificado problemas que expondremos en la presentación, y que quizás den lugar a un debate interesante sobre cómo se podrían solucionar, o cómo se podrían incorporar a un modelo más perfecto de Oficina que pudiera ser replicable en otras Universidades con mayor provecho.

-   Web del proyecto: <https://ofilibre.gitlab.io>

-   Público objetivo:

> Directamente, cualquier persona interesada en la cultura libre que tenga una relación activa con una Universidad. Indirectamente, cualquier persona interesada en el fomento de la cultura libre y el software libre en instituciones complejas.

## Ponente:

-   Nombre: Florencia Claes, Francisco Gortázar y Jesús González Barahona

-   Bio:

> Florencia Claes, Francisco Gortázar y Jesús González Barahona son coordinadores de la OfiLibre. Los tres tiene una amplia experiencia en los mundos de la cultura libre y el software libre, y naturalmente, en el funcionamiento de la OfiLibre de la URJC. Los tres tienen también amplia experiencia en la participación con ponencias en congresos y jornadas.

### Info personal:

-   Web personal: <https://ofilibre.gitlab.io>
-   Mastodon (u otras redes sociales libres): <https://floss.social/@OfiLibreURJC>
-   Twitter: <https://twitter.com/OfiLibreURJC>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
