---
layout: 2022/post
section: proposals
category: talks
author: Almudena García
title: Un novato en un proyecto legacy
---

# Un novato en un proyecto legacy

> En esta charla hablaremos sobre las experiencias producidas durante el desarrollo del proyecto Hurd SMP: mi primer proyecto en desarrollo de sistemas operativos, con un código legacy proveniente de los años 80 y 90, y con muchos conceptos altamente específicos que había que aprender sobre la marcha.<br><br>
Comentaremos los problemas que supuso este proyecto, tanto a nivel de conocimientos y experiencia, como de desarrollo; y cómo se han logrado solventar estos problemas gracias a la comunidad (y con mucha paciencia :D )

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> En este charla hablaremos del proceso de desarrollo del proyecto Hurd SMP pero, en lugar de centrarnos en detalles técnicos, nos centraremos en el aspecto mas humano: los sorprendentes comienzos del proyecto, con sus aciertos y desventuras, los problemas encontrados (documentación errónea), información proveniente de la propia comunidad...<br><br>
Y como, a través de la ayuda de la comunidad, tanto de los propios desarrolladores como de otros contribuidores, se fueron logrando solventar, logrando despertar el interés de los desarrolladores de Hurd, así como difundir el desarrollo y funcionamiento de este sistema operativo a nuevos usuarios y desarrolladores.<br><br>
La charla tendrá ciertos tintes cómicos, intentando mostrar el lado mas humano de este proyecto de forma divertida y amena.

-   Web del proyecto: <http://hurdsmp.wordpress.com>

## Ponente:

-   Nombre: Almudena García

-   Bio:

> Graduada en Ingeniería Informática por la Universidad de Huelva, en la especialidad de Ingeniería de Computadores.<br><br>
Presidenta fundadora de la Asociación de Software Libre de la Universidad de Huelva, en la cual imparto talleres sobre software libre a estudiantes y profesores.<br><br>
Actualmente soy profesora sustituta interina en la Universidad de Huelva, en el Departamento de Tecnologías de la Información, donde imparto varias asignaturas relacionadas con la programación en los grados de Ingeniería Informática e Ingeniería Electrónica.

### Info personal:

-   Web personal: <http://hatsuit.wordpress.com>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/almuhs> / <https://gitlab.com/AlmuHS>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
