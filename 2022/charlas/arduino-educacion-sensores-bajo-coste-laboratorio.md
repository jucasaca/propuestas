---
layout: 2022/post
section: proposals
category: talks
author: Beatriz Padín Romero
title: Arduino en educación&#58 sensores de bajo coste en el laboratorio de Física
---

# Arduino en educación: sensores de bajo coste en el laboratorio de Física

> Gracias a la placa controladora Arduino, el uso y la programación de sensores es sencillo y económicamente asequible. Esto, unido a la gran variedad de sensores disponibles, hace sumamente atractiva su utilización en contextos educativos. En esta charla mostraremos los beneficios didácticos que ofrece el uso de sensores con la placa Arduino en la clase de Física de ESO y Bachillerato.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> La aparición de la placa controladora Arduino ha conseguido algo que hace pocos años era impensable: ha democratizado el prototipado electrónico de objetos interactivos, un mundo que antes estaba reservado a ingenieros electrónicos. La revolución introducida por Arduino ha llegado incluso a la educación, donde su uso para aprender conceptos de electrónica y programación en la asignatura de Tecnología está muy extendido. Sin embargo, consideramos que existe otro ámbito educativo en el que la placa Arduino aporta numerosos beneficios didácticos: el uso de sensores en la clase de Física.<br><br>
Los sensores son una herramienta con la que se pueden tomar medidas en las actividades de laboratorio; además, pueden ser utilizados para ilustrar conceptos físicos o para mostrar relaciones entre magnitudes de manera práctica. Pero su potencial educativo no se limita a su mera utilización como aparato de medida. Si queremos que nuestros alumnos sean tecnológicamente competentes debemos fomentar que sean creadores, y no únicamente consumidores, de tecnología. Un paso hacia la consecución de este objetivo es hacer que ellos mismos diseñen y monten los circuitos de conexión del sensor y programen el comportamiento del microcontrolador.<br><br>
La introducción de sensores programados con Arduino, además de apoyar los contenidos curriculares de la asignatura de Física, promueve que los estudiantes adquieran competencias en el uso de herramientas computacionales como la programación científica que, aunque no está incluida en el currículo de las asignaturas de ciencias en Secundaria, es una herramienta con la que muchos alumnos se encontrarán en sus estudios universitarios e, incluso, en su futura vida laboral. También añade un factor motivador, ya que los alumnos descubren  la tecnología que se esconde detrás de los aparatos de medida utilizados en el laboratorio y “fabrican” los suyos propios. Y por último, aunque no menos importante, con el uso de Arduino estamos promoviendo la utilización de tecnologías libres como parte de la alfabetización digital de los estudiantes de secundaria.

-   Público objetivo:

> Profesores de ciencias de Enseñanza Secundaria, o cualquier persona interesada en el uso de herramientas tecnológicas en educación.

## Ponente:

-   Nombre: Beatriz Padín Romero

-   Bio:

> Licenciada en Física por la Universidad de Santiago de Compostela y Máster en periodismo científico y comunicación científica por la UNED, es profesora de Física y Química y Tecnología en el Colexio M. Peleteiro de Santiago de Compostela.<br><br>
Ha impartido charlas y talleres para profesores en diferentes foros educativos, tanto nacionales como en otros países europeos, sobre el uso de herramientas tecnológicas en la enseñanza de las ciencias en Secundaria: en el marco del proyecto europeo Coding in STEM Education (en Viena, Austria, y en Rovereto, Italia); en el Salón de Tecnología e Innovación Educativa - SIMO (Madrid); con la Institución Galega da Ciencia - Igaciencia en varias ediciones del Congreso de software libre e Foro Libre para Educación (Santiago de Compostela); con el Centro Autonómico de Formación e Innovación (CAFI) de la Xunta de Galicia (en varias localidades gallegas); en el I Congreso de Didáctica de la Química (Santiago de Compostela), y en la VIII Jornada de Usuarios de R en Galicia (Santiago de Compostela). En la Inspiring Science Education Conference celebrada en Atenas (Grecia) presentó un trabajo por el que se le concedió el segundo premio en el concurso Inspiring Science Education Scenario Competition.<br><br>
En la Revista Española de Física de la Real Sociedad Española de Física publicó dos artículos sobre el uso de herramientas computacionales en la enseñanza de la Física en Bachillerato. Es coautora de una unidad didáctica de la publicación Coding in STEM Education editada por la plataforma europea Science On Stage.<br><br>
Como profesora del Colexio M. Peleteiro fue tutora de grupos de alumnos con los que participó en ferias y concursos científicos gallegos y nacionales, como la Maker Faire Galicia, la feria científica Galiciencia o el Finde Científico organizado por la Fundación Española para la Ciencia y la Tecnología (FECYT). En la tercera edición del concurso Audi Creativity Challenge fue mentora del equipo que obtuvo el primer premio.

### Info personal:

-   Twitter: <https://twitter.com/bpadinr>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
