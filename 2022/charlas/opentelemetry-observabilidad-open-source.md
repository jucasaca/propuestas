---
layout: 2022/post
section: proposals
category: talks
author: Israel Blancas
title: OpenTelemetry la observabilidad desde el Open Source
---

# OpenTelemetry la observabilidad desde el Open Source

> ¿Has oido hablar de observabilidad? ¿Quieres saber qué es? ¿Y qué herramientas abiertas hay para implementarla en mis sistemas? Es todo lo que te cuento durante esta charla.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> Durante esta charla hablaré sobre observabilidad: por qué es importante y por qué se está poniendo tanto foco en ella. ¿Cual es el ecosistema alrededor de todo este tema? Hablaré de OpenTelemetry y otros proyectos como Jaeger o Elasticsearch, totalmente Open Source y que tú también puedes utilizar en tu plataforma.

-   Web del proyecto: <https://opentelemetry.io/>

-   Público objetivo:

> Todo aquel que desee conocer qué es la observabilidad y qué herramientas puede utilizar. La charla se puede seguir con un nivel muy básico en conocimientos de desarrollo de software o gestión de sistemas.

## Ponente:

-   Nombre: Israel Blancas

-   Bio:

> Quality Engineer @ Red Hat. GDE in Google Cloud. ¡Me encanta organizar y dar charlas y talleres tecnológicos o sobre el sector!

### Info personal:

-   Web personal: <http://iblancasa.com/>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@iblancasa>
-   Twitter: <https://twitter.com/iblancasa>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/iblancasa>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
