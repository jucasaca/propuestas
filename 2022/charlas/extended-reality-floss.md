---
layout: 2022/post
section: proposals
category: talks
author: inventadero
title: eXtended Reality FLOSS [Estado del arte]
---

# eXtended Reality FLOSS [Estado del arte]

> La Realidad eXtendida, los "metaversos" y toda la industria alrededor sufre un acaparamiento privativo muy fuerte. Este es un intento de recopilar y plantear alternativas Libres sobre el Hardware, Software y entornos Virtuales. Así como lugar común de conexión para futuras colaboraciones respecto al tema.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial

-   Descripción:

> La idea de esta propuesta es recopilar información en un Kanban de Taiga del estado del arte de la cuestión de la Realidad Extendida con Software Libre de forma colaborativa, y proponer proyectos y sinergias varias.<br><br>
Este entorno del XR, que quizás marque un nuevo paradigma, se encuentra acaparado por las "GAFAM", fagocitando proyectos y empresas. Existen iniciativas Libres de Capas de Realidad aumentada, como https://www.openarcloud.org/. Los temas del Kanban pudieran estructurarse +/- así:
* Realidad extendida (VR/AR/etc..)
* Hardware Libre XR
* Software Libre XR
* Hackear HeadSets (p.ej: las Oculus compradas por faszistbook)
* Open Augmented Layers.
* XR en Educación Pública y Libre.

-   Web del proyecto: <https://tree.taiga.io/project/inventadero-xr-floss-estado-del-arte/kanban>

-   Público objetivo:

> FLOSSers y personas, interesad@s en el tema, tengan el perfil que tengan ;~)

## Ponente:

-   Nombre: inventadero

-   Bio:

> Dibüjante, enamorado de la mar, la geometría y otras libertades. Blenderita, siempre aprendiendo y profe variopinto:
* Dibujo técnico
* Construcción Decorados Teatro
* Técnicas 3D Escuela Arte
* Talleres VR (Charleta eslibre 2021)
* Actualmente liado con tesis sobre FLOSS/EducaciónXR/Modelado3D

### Info personal:.

-   Web personal: <https://8d2.es/>
-   Mastodon (u otras redes sociales libres): <https://qoto.org/@inventadero>
-   Twitter: <https://twitter.com/@inventadero>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/inventadero>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
