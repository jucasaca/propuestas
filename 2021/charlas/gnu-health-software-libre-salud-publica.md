---
layout: 2021/post
section: proposals
category: talks
author: Luis Falcón
title: GNU Health como modelo de Software Libre en el sistema de Salud Pública
---

El proyecto GNU Health es un ecosistema de salud digital libre, que tiene como objetivo mejorar el acceso del ciudadano al sistema de salud y ayuda a los gobiernos en los programas de promoción de la salud y prevención de la enfermedad, con especial énfasis en atención primaria y medicina social.

El ecosistema GNU Health es un proyecto de la ONG humanitaria GNU Solidario, focalizada en la medicina social.

## Formato de la propuesta

- Charla plenaria

## Descripción

GNU Health es, principalmente,  un proyecto social que utiliza tecnología para mejorar el acceso y la gestión de la salud a nivel individuo, profesional, institución académica o de investigación y gobierno.

GNU Health es un proyecto de la ONG humanitaria GNU Solidario, focalizada en la medicina social, que utiliza Software Libre como medio para mejorar las condiciones de vida y de salud de los ciudadanos.

GNU Health ha sido adoptada por organizaciones como la Organización Mundial de la Salud, Universidad de Naciones Unidas (International Institute for Global Health), Cruz Roja (México).   

GNU Health se ha implementado en muchos países de Asia, Africa y Latinoamérica, desde clínicas, grandes hospitales, laboratorios de enfermedades infecciosas y sistemas de salud pública a nivel nacional.

En el contexto de la pandemia de COVID-19, GNU Health ha sido elegida en países como Argentina como observatorio en tiempo real COVID.

La Federación de GNU Health y sus componentes (gestión hospitalaria, laboratorio, epidemiología, sistema de gestión de salud personal [MyGNUHealth], bioinformática, GNU Health embedded..)

En GNU Health combinamos la medicina social y los determinantes socioeconómicos de la salud con los últimos avances en bioinformática y genética médica, en la investigación sobre cáncer, enfermedades raras y medicina de precisión.

El proyecto MyGNUHealth, componente del ecosistema GNU Health, es el el sistema de gestión de salud personal para escritorio y dispositivos móviles libres (como PinePhone), parte de la federación GNU Health y que integra plenamente y con un rol protagónico a la persona en el sistema de salud.

-   Web del proyecto: <https://www.gnuhealth.org>

## Público objetivo

- Público general
- Profesionales de la salud
- Académicos
- Administración de Salud Pública

## Ponente(s)

Luis Falcón es un médico, informático y activista español. Obtuvo el título en Ciencias de la Computación y Matemáticas en California State University, Northridge (USA) y de Medicina en el IUCS, Buenos Aires (Argentina). Posee un Masters en Genómica y Genética Médica por la Universidad de Granada (España). Es un activista social y de los derechos de los animales. Fundador y presidente de GNU Solidario, ONG focalizada en Medicina Social. Es el autor de GNU Health.

### Contacto(s)

-   Nombre: Luis Falcon
-   Email: <falcon@gnuhealth.org>
-   Web personal: <https://www.meanmicio.org>
-   Mastodon (u otras redes sociales libres): [https://diasp.org/i/71a45083ff3d](meanmicio@diasp.org)
-   Twitter: <https://twitter.com/meanmicio>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios
