---
layout: 2021/post
section: proposals
category: talks
author: Jesús Amieiro Becerra
title: PHP en 2021
---

Amado por unos, denostado por otros, PHP es uno de los lenguajes más importantes y más usados en la web, estando presente en un 79,2% de las webs. Esto se debe a que los CMS y frameworks más populares lo utilizan: WordPress, Drupal, Joomla, Magento, Prestashop, Laravel, Symfony... usan como base diferentes versiones de PHP.

En esta sesión explicaré el estado actual de PHP, los proyectos más influyentes y por qué goza de tanta popularidad.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Amado por unos, denostado por otros, PHP es uno de los lenguajes más importantes y más usados en la web, estando presente en un 79,2% de las webs. Esto se debe a que los CMS y frameworks más populares lo utilizan: WordPress, Drupal, Joomla, Magento, Prestashop, Laravel, Symfony,... usan como base diferentes versiones de PHP.

El guion propuesto para la sesión es el siguiente:

* Evolución: desde el nacimiento en 1994 hasta nuestros días, con la versión 8.0, pasando por las principales características de cada versión mayor.
* Versiones soportadas: 7.3, 7.4 y 8.0 y explicación del ciclo de soporte.
* Benchmarks: explicación de la mejora brutal en la versión 7.0, con gráficas que analizan el número de peticiones por segundo, la latencia y el uso de memoria.
* Popularidad: explicación del lugar que ocupa en el desarrollo en general y del que ocupa en el desarrollo web: 79,2 % de cuota de mercado.  
* Casos concretos. Detalle de 1 CMS (WordPress) con el 41.0 % de la cuota de mercado en la web (no solo en CMS) y de 2 frameworks con un amplio uso en el mercado (Laravel y Symfony).
* Presente y futuro. Reflexiones sobre el estado actual (curva de aprendizaje, barrera de entrada, escalabilidad, comunidad...) y sobre su futuro.

-   Web del proyecto: <https://www.php.net>

## Público objetivo

Desarrolladores web y programadores, estudiantes de programación.

## Ponente(s)

Soy Jesús Amieiro, ingeniero de telecomunicación y artesano de las TIC. Trabajo como director técnico en Quadralia. Me dedico al desarrollo de proyectos a medida, principalmente en el ámbito de la sanidad y de la trazabilidad marina. Como tecnologías me encuentro muy cómodo con Laravel, WordPress y, fundamentalmente, con el software libre. Participo o he participado en comunidades de PHP, WordPress y de software libre.

Publico semanalmente «La semana PHP», una newsletter sobre PHP.

### Contacto(s)

-   Nombre: Jesús Amieiro Becerra
-   Email:
-   Web personal: <https://www.jesusamieiro.com>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/JesusAmieiro>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/amieiro>

## Comentarios

Tenéis un listado con todas las charlas que he dado, incluyendo los vídeos, en <https://www.jesusamieiro.com/talks>.
