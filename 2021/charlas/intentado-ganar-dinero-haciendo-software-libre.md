---
layout: 2021/post
section: proposals
category: talks
author: José Manrique López de la Fuente
title: Intentado ganar dinero haciendo software libre
---

No, esto no es una charla formativa sobre como convertirse en persona millonaria montado la empresa de tus sueños, con un producto que la gente compra sin dudar pagando con criptomonedas, mientras tú y la gente que te acompaña en la aventura jugáis al billar, ping-pong, o futbolín (virtualmente a día de hoy). Esto es otra cosa...

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Lanzarse a montar una empresa ya es una aventura en si misma. Arriesgada, con momentos buenos y malos, pero desde luego toda una experiencia vital. Pero si además, se quieren ofrecer productos o servicios tecnológicos a empresas a través de software libre, la cosa se pone interesante.

¿Cómo surge la idea?¿Qué modelos de negocio se puede aplicar?¿Cómo vendemos esto?

Durante esta charla no pretendo dar ninguna lección sobre lo que hay que hacer. Para nada. Esta charla es una visión personal de la experiencia de trabajar en una empresa como [Bitergia](https://bitergia.com), y en concreto de lanzar un producto como [Cauldron Cloud](https://cloud.cauldron.io), que es 100% software libre.

¿En serio?¿Una solución 100% software libre para sacar datos que ya son públicos?¿Y por esto pagan empresas como Red Hat?¿Ha dicho Red Hat?

No es una charla de metodología para el éxito. No somos personas millonarias, al menos yo, por nuestros negocios y productos, ¿pero necesitamos serlo para ser felices e intentar tener un mundo mejor?

Me gustaría que la gente se llevase de la charla ideas para sus propios proyectos, y reflexiones para discutir incluso durante la propia charla.

-   Web del proyecto: <https://cauldron.io>

## Público objetivo

Personas emprendedoras, gestionando, o en vías de hacerlo, equipos, empresas o productos tecnológicos.

## Ponente(s)

Manrique es CEO y socio en Bitergia, y un apasionado del software libre, las comunidades de desarrollo y el surf.

Ingeniero industrial, con experiencia en I+D en CTIC, grupos de trabajo del W3C, Ándago Engineering y Continua Health Alliance. Ex-director ejecutivo de ASOLIF y ex-consultor experto de CENATIC.

Involucrado en varias comunidades relacionadas con el software libre y DevRel como GrimoireLab, CHAOSS y devrel.es.

Ha sido elegido AWS Data Hero y GitLab Community Hero.

### Contacto(s)

-   Nombre: José Manrique López de la Fuente
-   Email: <jsmanrique@bitergia.com>
-   Web personal: <https://jsmanrique.es>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@jsmanrique>
-   Twitter: <https://twitter.com/jsmanrique>
-   GitLab: <https://gitlab.com/jsmanrique>
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios
