---
layout: 2021/post
section: proposals
category: talks
author: Pablo Martínez Schroder
title: Una introducción a OpenStack controla tu cloud privada
---

Esta charla pretende ser una introducción a OpenStack y mostrar sus funcionalidades tanto como plataforma como producto que puedes instalar.

En estos tiempos en los que la oferta cloud cada vez está más concentrada en un grupo de grandes propuestas privativas OpenStack sigue ofreciendo una alternativa abierta y libre para aquellos que quieran operar su cloud privada o interactuar con una cloud ofrecida por un tercero.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Se trata de una introducción a OpenStack:

- Breve historia a la cloud en general, la cloud privada y OpenStack en particular
- Situación actual de OpenStack
- Componentes y casos de uso
- Ventajas de uso
- Incovenientes
- Resumen

-   Web del proyecto: <https://www.openstack.org/>

## Público objetivo

- Administradores de sistemas
- DevOps
- Gestores de infraestructura
- Personas con capacidad de decisión sobre infraestructura

## Ponente(s)

Me llamo Pablo y llevo más de 20 años trabajando como administrador de sistemas y en los últimos años he trabajado con OpenStack.

Desde que empecé mi vida laboral he trabajado con Linux principalmente, formándome en los entornos de GULs, tutoriales y darse cabezados. Mi labor ha sido casi siempre orientada a la administración de sistemas, aunque ello me ha llevado por distintos derroteros.

### Contacto(s)

-   Nombre: Pablo Martínez Schroder
-   Email: <pablo@docecosas.com>
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios
