---
layout: 2021/post
section: proposals
category: projects
title: Gaucho
---

Task launcher. Boost your productivity by running commands, scripts, and applications with one click.

Built with Electron and Vue.js.

-   Web del proyecto: <https://angrykoala.github.io/gaucho/>

### Contacto(s)

-   Nombre: Andrés Ortiz
-   Email: <angrykoala@outlook.es>
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/the_angry_koala>
-   GitLab: <https://gitlab.com/angrykoala>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/angrykoala>

## Comentarios
