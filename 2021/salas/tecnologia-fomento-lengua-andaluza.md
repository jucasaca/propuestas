---
layout: 2021/post
section: proposals
category: devrooms
community: Andalugeeks
title: Tecnología para el fomento de la lengua andaluza
---

## Descripción de la sala

En esta edición de eslibre, la comunidad de andalugeeks volvemos a proponer organizar una sala. Será un espacio enfocado tanto para las personas que pertenecen actualmente a la comunidad como a personas que se interesen por nuestros proyectos.

## Comunidad que la propone

#### Andalugeeks

AndaluGeeks somos un grupo de profesionales de la informática, la programación, el diseño gráfico y las TIC que creamos, desarrollamos y gestionamos proyectos tecnológicos de codigo libre y abierto alrededor de la lengua, la educación y la difusión de la cultura andaluza.

-   Web de la comunidad: <https://andaluh.es>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/andalugeeks>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo): https://github.com/andalugeeks/

### Contacto(s)

-   Nombre de contacto: Sergio Soto
-   Email de contacto:

## Público objetivo

Se trata de un espacio abierto a todas las personas (técnicas o no) que sientan curiosidad por el andaluz escrito. De especial interés en las temáticas: internacionalización (i18n) y localización (i10n) de aplicaciones, inteligencia artificial aplicada al desarrollo de herramientas para la transcripción inversa y devops  para la gestión de pipelinse de CI/CD e infraestructura basada en docker.

## Formato

### Resumen:

-   AndaluGeeks hoy: apps, novedades y comunidad Telegram
-   Impacto del Andalûh 2020: en música, redes, la tele, videojuegos, etc. y proyectos a los que estamos ayudando
-   Talleres técnicos
-   AndaluGeeks Roadmap: próximos proyectos y captación de developers

<h3 id="presentacion">11.00-12.15: AndaluGeeks, Software Libre para el aprendizaje y la difusión de la lengua andaluza</h3>

-   **Descripción**: presentación de la comunidad AndaluGeeks y las aplicaciones de
software libres actuales (andaluh-sdk, transcriptor web, móvil, bots, teclado,
Wikipedia) y las novedades: transcriptor de escritorio.

-   **Público objetivo**: personas no-técnicas que quieran conocer de las herramientas
libres de AndaluGeeks. Personas técnicas que quieran contribuir con los proyectos
actuales de AndaluGeeks.

-   **Contenidos y ponentes**:
    -   11.00-11.30: Aprende Andalûh, con Software Libre (Sergio Soto)
    -   11.30-12.15: El kit de desarrollo Andalûh SDK (J. Félix Ontañón)

<h3 id="impacto">12.30-13.45: Impacto del Andalûh en la cultura</h3>

-   **Descripción**: presentación en formato charla. Presentaremos la expansión de la propuesta EPA y el alcance que está teniendo en la cultura popular andaluza: música, televisión, videojuegos, etc.

-   **Público objetivo**: usuarias/os de las aplicaciones AndaluGeeks, que interesada en iniciar sus propias iniciativas culturales con andaluz escrito. Cualquier persona interesada en conocer el impacto que la escritura en Andalûh está teniendo.

-   **Contenidos y ponentes**:
    -   12.30-13.10: Repaso al impacto (Nono Feui)
    -   13.10-13.30: N'andalûh Fansub (Ana Aguilar y Andrés Padilla)
    -   13.30-13.45: Arxibo Luçero (Victor Manuel Clavijo)

<h3 id="talleres">16.00-17.15: Show me the code! Talleres!</h3>

-   **Descripción**: talleres técnicos para aprender de las tecnologías de desarrollo software libre empleadas en nuestras más recientes aplicaciones.

-   **Público objetivo**: developers que quieran aprender a desarrollar aplicaciones web para el escritorio con ElectronJS y extensiones para LibreOffice con Python.

-   **Contenidos**:
    -   16.00-16.45: ElectronJS, aplicaciones de escritorio multi-plataforma (Emilio Rivero)
    -   16.45-17:15: Extensiones LibreOffice (J. Félix Ontañón)

<h3 id="cfd">17.30-18.45: AndaluGeeks to the moon, Call for Developers!</h3>

-   **Descripción**: sesión abierta para developers y users de la comunidad AndaluGeeks.

-   **Público Objetivo**: contributors actuales de AndaluGeeks, newcomers que se quieran incorporar a alguno de nuestros proyectos actuales o futuros, y para users que tengan propuestas de nuevas aplicaciones. Si sabes de Python, Java, dotNET, PHP, JavaScript, Android, machine learning o realidad aumentada y quieres contribuir con AndaluGeeks pero no sabes cómo .. ¡apúntate y lo comentamos!

-   **Contenidos**:
    -   17.30-18.00: Experimentos en curso: machine learning, text-to-speech.
    -   18.00-18.45: Lluvia de ideas: diccionario, repositorio de voces, corrector ortográfico, plugin para WordPress, etc.

## Comentarios
