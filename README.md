## esLibre 2022

- Envío de propuestas (info en Español): <https://eslib.re/2022/info-propuestas/>
- Envío de propostas (info en Galego): <https://eslib.re/2022/info-propostas/>
- Submission of proposals (info in English): <https://eslib.re/2022/info-proposals/>


- Fecha límite extendida / data límite estendida / extended deadline: 7 de mayo / 7 de maio / May 7th
