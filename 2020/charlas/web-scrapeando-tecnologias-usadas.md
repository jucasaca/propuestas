---
layout: 2020/post
section: proposals
category: talks
title: Web scrapeando las tecnologías más usadas de la Web
---

¿Qué tecnologías son las más usadas en la Web? ¿Son ciertos los estudios que se publican? Mediante web scraping con Python se puede obtener esta información y mucho más.

## Formato de la propuesta

Indicar uno de estos:

-   [x]  Charla (25 minutos)
-   [ ]  Charla relámpago (10 minutos)

## Descripción

Partiendo de dos fuentes de información: el millón de página web más accedidas según Alexa y todos los sitios web registrados en España según Red.es, se ha realizado un web scraping sobre las mismas para posteriormente procesarlas con el analizador Wappalyzer, extrayendo así la información sobre todas las tecnologías usadas en esos sitios.

Entre ellas se destacan:

-   Lenguajes de programación
-   Bases de datos
-   Servidores web
-   Frameworks frontend/backend/CSS
-   CMS y sus correspondientes plugins

Con esos datos introducidos en una base de datos MongoDB, se generan las gráficas por conjuntos de tecnologías web. La idea de la charla es dar a conocer datos actualizados de las tecnologías más usadas en la web tanto en el mundo como en España, lo que puede ser de interés para mucha gente; así como el cómo se ha hecho.

## Público objetivo

Toda aquella persona interesada en el uso de las tecnologías y sobre todo aquellas que diseñan planes formativos en las empresas TIC.

## Ponente(s)

**David Vaquero Santiago** (Pepesan):

-   <https://www.linkedin.com/in/davidvaquero/>
-   <https://cursosdedesarrollo.com/>
-   <https://republicaweb.es/>

### Contacto(s)

-   **David Vaquero Santiago**: pepesan at gmail dot com | @pepesan

## Comentarios

En previsión a lo que pueda ocurrir con la agenda, la charla debería ser en sábado, porque lo más probable es que el viernes esté trabajando.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x]  Al menos una persona entre los que la proponen estará presente el día programado para la charla.
