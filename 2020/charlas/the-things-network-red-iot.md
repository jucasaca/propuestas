---
layout: 2020/post
section: proposals
category: talks
title: The Things Network&#58 una red IoT desplegada y mantenida por la ciudadanía
---

The Things Network es una iniciativa ciudadana global con el objetivo de desplegar una red IoT inalámbrica, descentralizada y tecnológicamente independiente, en la que los usuarios son a la vez los propietarios y los gestores de la red.

El despliegue de The Things Network se dinamiza mediante comunidades locales de voluntarios; actualmente existen alrededor de 500 comunidades en todo el mundo y más de 35 sólo en España, siendo la de Madrid la segunda más numerosa a nivel mundial.

The Things Network es una herramienta útil para la ciudadanía directamente, pero también indirectamente a través de sus ayuntamientos, porque permite despliegues con un coste muy inferior a los de otras alternativas disponibles en el sector de las telecomunicaciones.

## Formato de la propuesta

-   [x]  Charla (25 minutos)
-   [ ]  Charla relámpago (10 minutos)

## Descripción

En la charla explicaremos en qué consiste la iniciativa The Things Network para desplegar en régimen de procomún una red IoT abierta, libre, neutral, segura y útil.

Esta red se despliega actualmente mediante tecnología inalámbrica LoRa; explicaremos las características y posibilidades de esta técnica de modulación de radio que logra alcance del orden de kilómetros con consumos de energía mínimos (un mismo juego de baterías dura años).

También presentaremos algunos de los casos de uso sociales y de emprendedores de nuestra comunidad que ya funcionan sobre la red The Things Network.

## Público objetivo

Público en general; los conceptos expuestos son fácilmente accesibles precisamente porque es una red enfocada al despliegue ciudadano, que pretende empoderar al individuo y acercarle a la tecnología.

## Ponente(s)

-   **Juan Félix Mateos Barrado**
    -   Iniciador de la comunidad The Things Network Madrid
    -   Formador tecnológico del profesorado no universitario de la Comunidad de Madrid
    -   Profesor de asignaturas de IoT, AI, prototipado electrónico... en másters de la Universidad de Alcalá de Henares
    -   He presentado la red The Things Network en diversos actos como:
        -   <https://www.lanavemadrid.com/event/lorawan-practico-monta-y-configura-tu-propio-nodo/>
        -   <https://youtu.be/EYcq3XEq_2c>
        -   <https://oshwdem.org/oshwdem_ttn2/>

### Contacto(s)

-   **Juan Félix Mateos Barrado**: juanfelixmateos at gmail dot com

## Comentarios

Realizaremos también una propuesta de taller en la que se pondrá en práctica lo explicado en esta charla de introducción.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x]  Al menos una persona entre los que la proponen estará presente el día programado para la charla.
