---
layout: 2020/post
section: proposals
category: talks
title: RISC-V en asignaturas de "Arquitectura de Computadores"
---

Charlar en la que se explicara la motivación que ha llevado a decantarse por la arquitectura libre RISC-V para el temario de la asignatura de "Arquitectura de Computadores".

## Formato de la propuesta

-   [x]  Charla (25 minutos)
-   [ ]  Charla relámpago (10 minutos)

## Descripción

En esta charla se describe el curso de "Arquitectura de Computadores" que se imparte en 7 grados de la "Escuela Técnica Superior de Ingeniería de las Telecomunicaciones" de la "Universidad Rey Juan Carlos".

Se trata de un curso de seis créditos ECTS, 3 de teoría y 3 de prácticas, en el que se disponen de unas 30 sesiones de 2 horas entre clases teóricas y prácticas.

Después de llevar 6 cursos usando el microprocesador MIPS para impartir la asignatura, nos planteamos la posibilidad de cambiar a otro micro por varios motivos.

Primero, para revitalizar la asignatura y darle un nuevo impulso; por otro, para actualizarla. Es por eso que buscábamos un micro real y actual, pero también tenía que ser un micro del que se dispusiese de bibliografía para la parte de teoría, y de un simulador libre para la parte de prácticas.

Así, en un primer momento se planteó la posibilidad de usar ARM, sin embargo, el ascenso de RISC-V (open-source hardware) y la publicación de un libro basado en el mismo en 2018 y la existencia de un simulador libre, hacen que finalmente nos decantemos por este micro para rehacer la asignatura.

## Público objetivo

Principalmente docentes. Colateralmente, estudiantes, investigadores y empresas del sector.

## Ponente(s)

-   **Katia Leal Algara**: Profesora Titular interina en la "Escuela Técnica Superior de Ingeniería de las Telecomunicaciones" de la Universidad Rey Juan Carlos. Última presentación realizada el 5 de Marzo de 2020 en las Primeras Jornadas Red-RISCV de la Escola d'Enginyeria de la UAB.

-   **Juan González Gómez**: Profesor Ayudante Doctor en la "Escuela Técnica Superior de Ingeniería de las Telecomunicaciones" de la Universidad Rey Juan Carlos.

### Contacto(s)

-   **Katia Leal Algara**: katia.leal at urjc dot es
-   **Juan González Gómez**: obijuan.cube at gmail dot com

## Comentarios

Ninguno.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x]  Al menos una persona entre los que la proponen estará presente el día programado para la charla.
