---
layout: 2020/post
section: proposals
category: workshops
title: Taller de sistemas distribuidos basados en open source
---

Los sistemas distribuidos nos acompañan desde 1960 y, a partir de 1970, aplicaciones reales de ellos aparecieron: redes Ethernet, sistemas de correo o ARPANET (el predecesor de Internet).

Pero, ¿por qué son tan relevantes los sistemas distribuidos actualmente? Con la llegada de la computación en la nube y la necesidad del procesamiento de datos masivos (big data) los sistemas distribuidos han pasado a ser claves en estas aplicaciones que funcionan a nivel mundial. Miles de ordenadores con la capacidad de poder crecer se necesitan para satisfacer las demandas de estas aplicaciones.

Según podemos leer del Manifiesto Reactivo: _«Las aplicaciones actuales se desplieguen en cualquier cosa desde dispositivos móviles a clústers basados en la nube ejecutados por miles de procesadores multi-núcleo. Los usuarios esperan tiempos de respuesta de milisegundos y 100% de disponibilidad. Los datos se miden en Petabytes. Las demandas actuales, sencillamente, no estaban cubiertos por las arquitecturas de software anteriores.»_

## Objetivos a cubrir en el taller

En este taller las tecnologías claves necesarias para implementar estos sistemas distribuidos con open source serán presentadas, y algunas de ellas se mostrarán de forma práctica: gestores de recursos, planificadores, motores de ejecución, entornos de desarrollo o sistemas de almacenamiento.

El principal objetivo del taller es obtener una visión global del funcionamiento de los sistemas distribuidos. Se verá de forma práctica como se usan algunas de las tecnologías (open source) como Nomad/Kubernetes, Kafka, Spark o HDFS.

Se partirá de un pequeño clúster basados en máquinas virtuales que será gestionado por Nomad y/o Kubernetes (gestores de recursos y planificación) y sobre el que se ejecutará una ETL que leerá datos de forma distribuida, los publicará en Kafka y se procesarán de forma distribuida con Spark, dejando los resultados en el sistema de ficheros distribuido HDFS. El objetivo final es tener escalabilidad potencial infinita.

## Público objetivo

Todo desarrollador o administrador de sistemas que quieran entender los requisitos que imponen el trabajar con sistemas distribuidos, en especial aquellos perfiles que apuesten por devops.

## Ponente(s)

**Alvaro del Castillo San Felix**. En la pasada edición de esLibre presentó el taller de big data que será una de las partes incluidas dentro de este nuevo taller, que tiene objetivos más ambiciosos.

### Contacto(s)

-   **Alvaro del Castillo San Felix**: adelcastillo at thingso2 dot com

## Prerrequisitos para los asistentes

Será necesario tener un equipo en el que se pueden desplegar contenedores Docker, siendo recomendable que esté basado en Linux.

## Prerrequisitos para la organización

Para el taller será necesario disponer de un proyector y conexión a Internet. Si además se dispone de ordenadores basados en Linux y con Docker instalado, mejor que mejor.

## Tiempo

El taller da pie a tener una duración flexible en función de cuanto se quiera cubrir y con que necesidad. Con 2 horas debería de ser suficiente para una introducción.

## Día

Preferentemente el viernes 5 de Junio.

## Comentarios

Ninguno.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/).
-   [x]  Al menos una persona entre los que proponen el taller estará presente el día programado para el mismo.
-   [x]  Acepto coordinarme con la organización de esLibre.
