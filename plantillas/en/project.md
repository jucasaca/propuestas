---
layout: 2021/post
section: proposals
category: projects
title: PROJECT-TITLE
---

[ABSTRACT: Short introduction and motivation of the proposal.]

-   Project website: [URL]

### Contact/s

-   Name: [NAME]
-   Email: [EMAIL]
-   Personal website: [URL]
-   Mastodon (or other free social networks): [URL]
-   Twitter: [URL]
-   Gitlab: [URL]
-   Portfolio or GitHub (or other collaborative code sites): [URL]

## Comments

[COMMENTS: Any other comment relevant to the organization.]

## Privacy preferences

(If you want your contact information to be anonymous, you can send us the proposals using the forms on the web: <https://eslib.re/2021/proposals/projects/>)

-   [x]  I give permission for my contact email to be published with the information of the project.
-   [x]  I give permission for my social networks to be published with the information from the project.

## Accepted conditions

-   [x]  I agree to follow the [conduct code](https://eslib.re/conduct/) and to ask those attending to comply with it.
